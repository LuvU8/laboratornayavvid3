﻿#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QString>

QString binToDec (QString num);
QString decToBin (QString num);
QString binToOct (QString num);
QString octToBin (QString num);
QString octToHex (QString num);
QString hexToOct (QString num);

#endif // FUNCTIONS_H
