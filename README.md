## LAB 3

Список задач, выполняемых программой:
* [x] Перевод числа из двоичной СС в десятичную
* [x] Перевод числа из десятичной СС в двоичную
* [x] Перевод числа из двоичной СС в восьмеричную
* [x] Перевод числа из восьмеричной СС в двоичную
* [x] Перевод числа из восьмеричной СС в шестнадцатеричную
* [x] Перевод числа из шестнадцетиричной СС в восьмеричную